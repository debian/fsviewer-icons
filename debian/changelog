fsviewer-icons (1.0-9) unstable; urgency=medium

  * QA upload.

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

  [ Petter Reinholdtsen ]
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.5.0 to 4.7.0.
  * Use wrap-and-sort -at for debian control files.
  * Use correct machine-readable copyright file URI.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 01 Dec 2024 13:46:54 +0100

fsviewer-icons (1.0-8) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #920061)
  * Using new DH level format. Consequently:
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Bumped Standards-Version to 4.5.0
      - Changed Priority from extra to optional

 -- Marcelo Vinicius Campos Amedi <marceloamedi@gmail.com>  Sat, 09 May 2020 15:29:10 -0300

fsviewer-icons (1.0-7) unstable; urgency=medium

  * debian/control
    - (Build-Depends): Update to debhelper 9.
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - (Format): Update from DEP5 to 1.0.
  * debian/watch
    - Add (Lintian fix).

 -- Jari Aalto <jari.aalto@cante.net>  Wed, 19 Oct 2016 07:59:30 +0300

fsviewer-icons (1.0-6) unstable; urgency=low

  * debian/compat
    - Update to 8.
  * debian/control
    - (Build-Depends): update to debhelper 8.
    - (Standards-Version): update to 3.9.2.
  * debian/copyright
    - Update to DEP 5.
  * debian/*.log
    - Delete temporary files.
  * xpm/{home,folder-link,folder}.xpm
    - Restore original files from source archive (FTBFS; Closes: #643122).

 -- Jari Aalto <jari.aalto@cante.net>  Tue, 27 Sep 2011 11:29:23 +0300

fsviewer-icons (1.0-5) unstable; urgency=low

  * New maintainer (Closes: #540999).
    - Move to packaging format: 3.0 (quilt).
  * debian/compat
    - New file.
  * debian/control
    - (Build-Depends): update to debhelper 7.1.
    - (Description): Mention formats XPM and TIFF.
    - (Depands): Add ${misc:Depends}.
    - (Standards-Version): update to 3.8.4.
  * debian/copyright
    - Author allowed to relicense under GPL-2+; adjust Copyright
      accordingly and move previous information under "old" section.
    - Add "Design Science License" URL.
    - (Upstream Author): Update address.
    - (It was downloaded from): Update Author's homepage.
  * debian/dirs
    - Add XPM and TIFF directories.
  * debian/fsviewer-icons.install
    - Move content from 'rules' for dh(1) to use.
  * debian/rules
    - Move to dh(1).
  * debian/source/format
    - New file.

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 24 May 2010 16:38:12 +0300

fsviewer-icons (1.0-4) unstable; urgency=low

  * Remove circular dependency on fsviewer (closes: #339914)

 -- Alain Schroeder <alain@debian.org>  Sat,  3 Jun 2006 12:12:57 +0200

fsviewer-icons (1.0-3) unstable; urgency=low

  * improved description (closes: #209499)

 -- Alain Schroeder <alain@debian.org>  Wed, 22 Oct 2003 16:40:44 +0200

fsviewer-icons (1.0-2) unstable; urgency=medium

  * The Build-Depends are actually Build-Depends-Indep... Thanks to lintian!

 -- Alain Schroeder <alain@debian.org>  Sat,  4 May 2002 18:10:46 +0200

fsviewer-icons (1.0-1) unstable; urgency=low

  * Initial Release.
  * Got permission from Author to relicense under the free "design
    science license". Included the license as dsl.txt.gz.

 -- Alain Schroeder <alain@debian.org>  Thu,  2 Aug 2001 21:52:32 +0200
